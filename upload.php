<?php
    include "headerSnippet.php";
    // File was uploaded and in a non-error state
    if (isset($_FILES['doc']) && ($_FILES['doc']['error'] == UPLOAD_ERR_OK)) {

        // Using a user-agent stream instead of simplexml_load_file is the result of a bug-hunting quest that turned out to not be my fault
        // I'm keeping this though in the off chance it will fix a bug I haven't had yet
        $context = stream_context_create(array(
            'http'=>array(
                'user_agent'=>'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11'
                )
             ));
        $file = file_get_contents($_FILES['doc']['tmp_name'],FALSE,$context);
        $xml = simplexml_load_string($file);

        $entities = $xml->xpath("//item");
        // There is only ever one location
        $position = $xml->xpath("//location")[0];
        // There is only ever one cgt
        $thisTime = $xml->xpath("//cgt")[0];
        // timespan converted to seconds from the YY DD HH MM SS format used by CGT for ease of comparisons
        $thisTimeSpan = $thisTime->seconds+
                        $thisTime->minutes*60+
                        $thisTime->hours*60*60+
                        $thisTime->days*24*60*60+
                        $thisTime->years*365*24*60*60;
        foreach ($entities as $key => $entity) {
            // Grab the entity to updated it
            $query = "SELECT OwnerID, TypeID, GalacticPosID, LocalPosID, YearsSince, DaysSince, HoursSince, MinutesSince, SecondsSince FROM Entities WHERE ID = " . 
                mysqli_real_escape_string($mysqli, $entity->entityID);
            $res = $mysqli->query($query)->fetch_assoc();
            
            // If the Entity does not already exist, create it and re-query for Entity with Default Data, set to sensible values which should always trigger update from scan conditions.
            if(!isset($res)){
                $query1 = "INSERT INTO Entities (ID) VALUES (" . mysqli_real_escape_string($mysqli, $entity->entityID) . ")";
                $temp = $mysqli->query($query1);
                print_r("There should be now");
                print_r($query1);
                print_r("Because of that");
                $res = $mysqli->query($query)->fetch_assoc();
            }
            
            $lastType = $res['TypeID'];
            $lastOwner = $res['OwnerID'];
            $lastGalactic = $res['GalacticPosID'];
            $lastLocal = $res['LocalPosID'];
            // The 0-based index of the 'YearsSince' column in the SELECT statement
            $lastTime = array_slice($res, 4);
            // timespan converted to seconds from the YY DD HH MM SS format used by CGT for ease of comparisons
            $lastTimeSpan = $lastTime['SecondsSince']+
                            $lastTime['MinutesSince']*60+
                            $lastTime['HoursSince']*60*60+
                            $lastTime['DaysSince']*24*60*60+
                            $lastTime['YearsSince']*365*24*60*60;
            // If this scan is more 'recent' in-world, update the entity with this data
            if ($lastTimeSpan < $thisTimeSpan) {
                $entitiesUpdate = "UPDATE Entities SET ";

                // Always update the name and image. There is little cost to this in-game (with names at any rate).
                $entitiesUpdate .= "Name = '" . mysqli_real_escape_string($mysqli, $entity->name) . "', ";
                $entitiesUpdate .= "ImageUrl = '" . mysqli_real_escape_string($mysqli, $entity->image) . "', ";

                // Get the type ID of the type shown by the scan
                $query = "SELECT ID FROM Types WHERE Name = '" . 
                    mysqli_real_escape_string($mysqli, $entity->typeName) . "'";
                $res = $mysqli->query($query)->fetch_assoc();
                // If result set is empty, type does not already exist in DB and we should add it
                if (!isset($res)) {
                    $query1 = "INSERT INTO Types (Name) VALUES ('" . 
                        mysqli_real_escape_string($mysqli, $entity->typeName) . "')";
                    $mysqli->query($query1);
                    // Now we added it, we should retrieve the ID
                    $res = $mysqli->query($query)->fetch_assoc();
                }
                // If the type ID shown by the scan is not the type ID in the entity record, update it
                if($res['ID']!= $lastType) {
                    $entitiesUpdate .= "TypeID = " . mysqli_real_escape_string($mysqli, $res['ID']) . ", ";
                }

                // Get the owner ID of the owner shown by the scan
                $query = "SELECT ID FROM Owners WHERE Name = '" . 
                    mysqli_real_escape_string($mysqli, $entity->ownerName) . "'";
                $res = $mysqli->query($query)->fetch_assoc();
                // If result set is empty, owner does not already exist in DB and we should add them
                if (!isset($res)) {
                    $query1 = "INSERT INTO Owners (Name) VALUES ('" . 
                        mysqli_real_escape_string($mysqli, $entity->ownerName) . "')";
                    $mysqli->query($query1);
                    // Now we added them, we should retrieve their ID
                    $res = $mysqli->query($query)->fetch_assoc();
                }
                // If the owner ID shown by the scan is not the owner ID in the entity record, update it
                if($res['ID']!= $lastOwner) {
                    $entitiesUpdate .= "OwnerID = " . mysqli_real_escape_string($mysqli, $res['ID']) . ", ";
                }

                // Get the Galactic Coordinate ID of the co-ordinate shown in the scan
                $query = "SELECT ID FROM GalacticCoords WHERE x = " . 
                    mysqli_real_escape_string($mysqli, $position->galX) . " and y = " . 
                    mysqli_real_escape_string($mysqli, $position->galY);
                    print_r($query);
                    echo "<hr />";
                $res = $mysqli->query($query)->fetch_assoc();
                // If result set is empty, galactic position does not already exist and we should add it
                if (!isset($res)) {
                    $query1 = "INSERT INTO GalacticCoords (x,y) VALUES (" . 
                        mysqli_real_escape_string($mysqli, $position->galX) . ", " . 
                        mysqli_real_escape_string($mysqli, $position->galY) . ")";
                    $mysqli->query($query1);
                    // Now we added it, we should retrieve the ID
                    $res = $mysqli->query($query)->fetch_assoc();
                }
                // If the Galactic Coordinate ID shown by the scan is not the Galactic Coordinate ID in the entity record, update it
                if($res['ID']!= $lastGalactic) {
                    $entitiesUpdate .= "GalacticPosID = " . mysqli_real_escape_string($mysqli, $res['ID']) . ", ";
                }

                // Get the Local Coordinate ID of the co-ordinate shown in the scan
                $query = "SELECT ID FROM LocalCoords WHERE x = " . 
                    mysqli_real_escape_string($mysqli, $position->sysX) . " and y = " . 
                    mysqli_real_escape_string($mysqli, $position->sysY);
                    print_r($query);
                    echo "<hr />";
                $res = $mysqli->query($query)->fetch_assoc();
                // If result set is empty, local position does not already exist and we should add it
                if (!isset($res)) {
                    $query1 = "INSERT INTO LocalCoords (x,y) VALUES (" . 
                        mysqli_real_escape_string($mysqli, $position->sysX) . ", " . 
                        mysqli_real_escape_string($mysqli, $position->sysY) . ")";
                    $mysqli->query($query1);
                    // Now we added them, we should retrieve the ID
                    $res = $mysqli->query($query)->fetch_assoc();
                }
                // If the Local Coordinate ID shown by the scan is not the Local Coordinate ID in the entityt record, update it
                if($res['ID']!= $lastLocal) {
                    $entitiesUpdate .= "LocalPosID = " . mysqli_real_escape_string($mysqli, $res['ID']) . ", ";
                }

                // Update the time of last update for the entity to the time of the scan
                $entitiesUpdate .= "YearsSince = " . mysqli_real_escape_string($mysqli, $thisTime->years) . ", ";
                $entitiesUpdate .= "DaysSince = " . mysqli_real_escape_string($mysqli, $thisTime->days) . ", ";
                $entitiesUpdate .= "HoursSince = " . mysqli_real_escape_string($mysqli, $thisTime->hours) . ", ";
                $entitiesUpdate .= "MinutesSince = " . mysqli_real_escape_string($mysqli, $thisTime->minutes) . ", ";
                $entitiesUpdate .= "SecondsSince = " . mysqli_real_escape_string($mysqli, $thisTime->seconds);

                // Filter by Entity ID
                $entitiesUpdate .= " WHERE ID = " . mysqli_real_escape_string($mysqli, $entity->entityID);
                $mysqli->query($entitiesUpdate);
            }
        }
        echo "<p>Thank you for your contribution.</p>";
    } else {
        echo "<p>Please provide a valid XML file for upload</p>";
    } 
?>