-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 01, 2017 at 03:05 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ShippingRecords`
--
CREATE DATABASE IF NOT EXISTS `ShippingRecords` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `ShippingRecords`;

-- --------------------------------------------------------

--
-- Table structure for table `Entities`
--

CREATE TABLE IF NOT EXISTS `Entities` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ImageUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TypeID` int(11) NOT NULL,
  `OwnerID` int(11) NOT NULL,
  `GalacticPosID` int(11) NOT NULL,
  `LocalPosID` int(11) NOT NULL,
  `YearsSince` int(11) NOT NULL,
  `DaysSince` int(11) NOT NULL,
  `HoursSince` int(11) NOT NULL,
  `MinutesSince` int(11) NOT NULL,
  `SecondsSince` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  KEY `Entities_ibfk_1` (`OwnerID`),
  KEY `Entities_ibfk_2` (`GalacticPosID`),
  KEY `Types` (`TypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `GalacticCoords`
--

CREATE TABLE IF NOT EXISTS `GalacticCoords` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `y` int(11) NOT NULL,
  `x` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `LocalCoords`
--

CREATE TABLE IF NOT EXISTS `LocalCoords` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `y` int(11) NOT NULL,
  `x` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Owners`
--

CREATE TABLE IF NOT EXISTS `Owners` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Types`
--

CREATE TABLE IF NOT EXISTS `Types` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
