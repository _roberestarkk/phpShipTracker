<?php 

        $linkQuery = $_SERVER['SCRIPT_NAME']."?";
        foreach ($_GET as $key => $value) {
            // Strip out the page value but keep rest of query same
            if(!($key=="page")){
                $linkQuery .= $key . "=" . $value . "&";
            }
        }
        ?>
        <!-- Create link to the first page of results (if applicable) -->
        <?php if($pageNum!=1) { ?>
            <a href="<?php echo $linkQuery . 'page=' . 1 ?> ">First</a>
        <?php } else { ?> 
            First 
        <?php } ?> | 
        <!-- Create link to the previous page of results (if applicable) -->
        <?php if($pageNum-1>=1) {?>
            <a href="<?php echo $linkQuery . 'page=' . ($pageNum-1) ?> ">Prev</a> 
        <?php } else { ?>
            Prev 
        <?php } ?> | 
        <!-- Create box and button for jumping to a specific page -->
        <form class="paginationForm" action="<?php $_SERVER['SCRIPT_NAME'].$_SERVER['REQUEST_URI'] ?>" method="get">
            <input type="number" min="1" max="<?php echo ceil($totalRows/$showAmount) ?>" name="page" value="<?php echo $pageNum ?>" id=""> / <?php echo ceil($totalRows/$showAmount) ?>
            <input type="submit" value="Go">
        </form> | 
        <!-- Create link to the next page of results (if applicable -->
        <?php if($pageNum+1<=ceil($totalRows/$showAmount)) {?>
            <a href="<?php echo $linkQuery . 'page=' . ($pageNum+1) ?> ">Next</a> 
        <?php } else { ?>
            Next 
        <?php } ?> | 
        <!-- Create link to the last page of results (if applicable) -->
        <?php if($pageNum!=ceil($totalRows/$showAmount)) { ?>
            <a href="<?php echo $linkQuery . 'page=' . ceil($totalRows/$showAmount) ?> ">Last</a>
        <?php } else { ?>
            Last 
        <?php } ?>
        <br />
        <!-- Create links for specifying the number of results per page -->
Results per page: <?php
        $linkQuery = $_SERVER['SCRIPT_NAME']."?";
        foreach ($_GET as $key => $value) {
            // Strip out the perPage value but keep rest of query same
            if(!($key=="perPage")){
                $linkQuery .= $key . "=" . $value . "&";
            }
        }
?><a href="<?php echo $linkQuery . "perPage=10" ?>">10</a>, 
<a href="<?php echo $linkQuery . "perPage=20" ?>">20</a>, 
<a href="<?php echo $linkQuery . "perPage=50" ?>">50</a>, 
<a href="<?php echo $linkQuery . "perPage=100" ?>">100</a> 
<br />
<br>