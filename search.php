<?php include "headerSnippet.php"; ?>
    <!-- Show search results -->
    <?php
    // Get the number to show per page or set to default if not given
    if(!isset($_GET['perPage']) || $_GET['perPage'] == ""){
        $showAmount = 10;
        $_GET['perPage'] = 10;
    } else {
        switch ((int)$_GET['perPage']) {
            case 10:
                $showAmount = 10;
                break;
            case 20:
                $showAmount = 20;
                break;
            case 50:
                $showAmount = 50;
                break;
            case 100:
                $showAmount = 100;
                break;
            default:
                $showAmount = 10;
                break;
        }
    }

    // Get the ordering criteria or set to default if not given
    if(!isset($_GET['orderBy']) || $_GET['orderBy'] == ""){
        $orderBy = "name";
        $_GET['orderBy'] = "name";
    } else {
        switch ($_GET['orderBy']) {
            case 'name':
                $orderBy = 'name';
                break;
            case 'time':
                $orderBy = 'yearssince, dayssince, hourssince, minutessince, secondssince';
                break;
            case 'position':
                $orderBy = 'gp.x,gp.y,lc.x,lc.y';
                break;
            case 'owner':
                $orderBy = 'owner';
                break;
            case 'type':
                $orderBy = 'type';
                break;
            default:
                $orderBy = 'name';
                break;
        }
    }
    // Get the ordering direction or set to default if not given
    if(!isset($_GET['sortOrder']) || $_GET['sortOrder'] == ""){
        $orderDirection = "ASC";
        $_GET['sortOrder'] = "ASC";
    } else {
        if($_GET['sortOrder'] == "DESC"){
            $orderDirection="DESC";
        } else {
            $orderDirection = "ASC";
        }
    }
    // Get the number of the page to display or set to default if not given
    if(!isset($_GET['page']) || $_GET['page'] == ""){
        $pageNum = 1;
        $_GET['page'] = 1;
    } else {
        if($_GET['page'] >= 1)
            $pageNum = (int) $_GET['page'];
        else
            $pageNum = 1;
    }

    // Start forming the query, specify the data and the joining of tables.
    $query = "SELECT SQL_CALC_FOUND_ROWS e.Name as Name, ImageUrl, YearsSince, DaysSince, HoursSince, MinutesSince, SecondsSince, gp.x as GalX, gp.y as GalY, lc.x as SysX, lc.y as SysY, o.Name as Owner, t.Name as Type FROM Entities e
    INNER JOIN GalacticCoords gp ON e.GalacticPosID = gp.ID
    INNER JOIN LocalCoords lc ON e.LocalPosID = lc.ID
    INNER JOIN Owners o on e.OwnerID = o.ID
    INNER JOIN Types t on e.TypeID = t.ID";

    // Start building an array of conditions (WHERE x AND Y and Z AND etc.) for the co-ordinate searching (if applicable) as those are combined conditions
        $conditions = array();
        // If galRadius is set, search for the galx co-ord between provided point + and - the provided radius, and/or the same for y.
        if (isset($_GET['galRadius']) && $_GET['galRadius'] != "") {
            if (isset($_GET['galX']) && $_GET['galX'] != "") {
                array_push($conditions , "gp.x BETWEEN " . $_GET['galX'] ."-". $_GET['galRadius'] . " AND " . $_GET['galX'] ."+". $_GET['galRadius'] . " ");
            }
            if (isset($_GET['galY']) && $_GET['galY'] != "") {
                array_push($conditions , "gp.y BETWEEN " . $_GET['galY'] ."-". $_GET['galRadius'] . " AND " . $_GET['galY'] ."+". $_GET['galRadius'] . " ");
            }
        }
        // If galRadius is not set, search for the galx co-ord provided, and/or the same for y.
        // NB: This implementation theoretically supports a comma separated list of co-ordinates, but the web interface does not.
        else {
            if (isset($_GET['galX']) && $_GET['galX'] != ""){
                array_push($conditions, "gp.x in (" . $_GET['galX'] . ")");
            }
            if (isset($_GET['galY']) && $_GET['galY'] != ""){
                array_push($conditions, "gp.y in (" . $_GET['galY'] . ")");
            }
        }
        // If locRadius is set, search for the locx co-ord between provided point + and - the provided radius, and/or the same for y.
        if (isset($_GET['locRadius']) && $_GET['locRadius'] != "") {
            if (isset($_GET['locX']) && $_GET['locX'] != "") {
                array_push($conditions , "lc.x BETWEEN " . $_GET['locX'] ."-". $_GET['locRadius'] . " AND " . $_GET['locX'] ."+". $_GET['locRadius'] . " ");
            }
            if (isset($_GET['locY']) && $_GET['locY'] != "") {
                array_push($conditions , "lc.y BETWEEN " . $_GET['locY'] ."-". $_GET['locRadius'] . " AND " . $_GET['locY'] ."+". $_GET['locRadius'] . " ");
            }
        }
        // If locRadius is not set, search for the locx co-ord provided, and/or the same for y.
        // NB: This implementation theoretically supports a comma separated list of co-ordinates, but the web interface does not.
        else {
            if (isset($_GET['locX']) && $_GET['locX'] != ""){
                array_push($conditions, "lc.x in (" . $_GET['locX'] . ")");
            }
            if (isset($_GET['locY']) && $_GET['locY'] != ""){
                array_push($conditions, "lc.y in (" . $_GET['locY'] . ")");
            }
        }

        // After having determined which co-ordinates (if any) are to be search conditions, craft the search statement.
        if(!empty($conditions)){
        foreach ($conditions as $key => $cond) {
            if($key == 0){
                $query .= " WHERE ";
            } else {
                $query .= " AND ";
            }
            $query .= mysqli_real_escape_string($cond);
        }
    }
    // If searching by name of entity, get entity name provided and search it (similar for owner and type)
    else if (isset($_GET['entityName'])){
        $query .= " WHERE e.Name like '%" . mysqli_real_escape_string($mysqli, $_GET['entityName']) . "%'";
    }
    else if (isset($_GET['ownerName'])){
        $query .= " WHERE o.Name like '%" . mysqli_real_escape_string($mysqli, $_GET['ownerName']) . "%'";
    }
    else if (isset($_GET['entityType'])){
        $query .= " WHERE t.Name like '%" . mysqli_real_escape_string($mysqli, $_GET['entityType']) . "%'";
    }

    // Order the results of the query by the desired column (or the default) and limit the number of results returned for pagination purposes.
    $query .= " ORDER BY " . $orderBy . " " . $orderDirection . " LIMIT " . ($pageNum-1)*$showAmount . ", " . $showAmount;
    
    $res = $mysqli->query($query)->fetch_all();
    $totalRows = $mysqli->query("SELECT FOUND_ROWS()")->fetch_array()[0];
    if(empty($res)){
        echo "<p>Sorry, no results found</p>";
    } else {
        include "tableNav.php";

        // Display the results in a table
    echo '<table class="centeredTable bordered">';
    // The headers are in the same order as the select statement to allow incremental access
    include "tableSorting.php";
    echo "<tr>";
    // setupHeader implements sorting for that column
    setupHeader('name',$orderBy, $orderDirection);
    echo "<th>Image</th>";
    setupHeader('time',$orderBy, $orderDirection);
    setupHeader('position',$orderBy, $orderDirection);
    setupHeader('owner',$orderBy, $orderDirection);
    setupHeader('type',$orderBy, $orderDirection);
    echo "</tr>";
    foreach ($res as $key => $entity) {
        echo "<tr>";
            echo "<td style='text-align: center;'>" . $entity[0] . "</td>";
            echo '<td><img src="' . $entity[1] . '"></img></td>';
            // This is a nested table to provide data formatting
            echo "<td><table><tr><td>Year:</td><td style='text-align: right;'>" . $entity[2] . "</td></tr> <tr><td>Day:</td><td style='text-align: right;'>" . $entity[3] . "</td></tr><tr><td>Hour:</td><td style='text-align: right;'>" . $entity[4] . "</td></tr><tr><td>Minute:</td><td style='text-align: right;'>" . $entity[5] . "</td></tr><tr><td>Second:</td><td style='text-align: right;'>" . $entity[6] . "</td></tr></table></td>";
            echo "<td style='text-align: center;'>Galactic (X,Y): <br />(" . $entity[7] . ", " . $entity[8] . ") <br /><br />System (X,Y): <br />(" . $entity[9] . ", " . $entity[10] . ")</td>";
            echo "<td style='text-align: center;'>" . $entity[11] . "</td>";
            echo "<td style='text-align: center;'>" . $entity[12] . "</td>";
        echo "</tr>";
    }
    echo "</table>";
    echo "<br />";
    include "tableNav.php";
}
?> 